#!/bin/bash

echo mkdir -p "./bin/${installname}"
mkdir -p "./bin/${installname}"

echo cp -r "/usr/local/spm/bin/${installname}" "./bin/"
cp -r "/usr/local/spm/bin/${installname}" "./bin/"

echo "/usr/local/spm/env/*.sh ./bin/${installname}/"
cp /usr/local/spm/env/*.sh "./bin/${installname}/" || exit 1
echo "cp /usr/local/spm/env/run${installname}.sh ./bin/${installname}/runenv.sh"
cp "/usr/local/spm/env/run${installname}.sh" "./bin/${installname}/runenv.sh" || exit 1
