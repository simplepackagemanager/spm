#!/bin/bash

if [ "$RUNFROMSHELL" != "" ];
then
    echo "Running from shell. Skipping apt-get install"
    return
fi

echo "deb http://deb.debian.org/debian $1 main" >> /etc/apt/sources.list
