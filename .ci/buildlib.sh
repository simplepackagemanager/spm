#!/bin/bash

export srcbranch="$2"
export envname="${1}_${2}"

cd /usr/local/spm || exit 1
./.ci/runretry.sh ./getsrc.sh "$1" || exit 1
./.ci/run.sh ./build.sh "$1" || exit 1
