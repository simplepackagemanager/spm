SRCURL=https://github.com/madler/zlib.git
DEFAULT_BRANCH="v1.2.11"

ENV_PATH="bin:lib:include"
ENV_LD_LIBRARY_PATH="lib"
ENV_PKG_CONFIG_PATH="lib/pkgconfig"
ENV_LDFLAGS_PATH="lib"
ENV_MANPATH="share/man"
ENV_CPPFLAGS_PATH="include"

ENABLE_SAME_DIR_BUILD="true"

BUILD_TYPE="configure"
