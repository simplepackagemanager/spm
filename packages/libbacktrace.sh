# original library broken in configure
#SRCURL=https://github.com/ianlancetaylor/libbacktrace.git
# using patched version
SRCURL=https://github.com/4144/libbacktrace.git

ENV_PATH="lib:include"
ENV_LD_LIBRARY_PATH="lib"
ENV_LDFLAGS_PATH="lib"
ENV_CPPFLAGS_PATH="include"

BUILD_TYPE="automake"
