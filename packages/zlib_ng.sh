SRCURL=https://github.com/Dead2/zlib-ng.git
DEFAULT_BRANCH="develop"

ENV_PATH="bin:lib:include"
ENV_LD_LIBRARY_PATH="lib"
ENV_PKG_CONFIG_PATH="lib/pkgconfig"
ENV_LDFLAGS_PATH="lib"
ENV_MANPATH="share/man"
ENV_CPPFLAGS_PATH="include"

BUILD_TYPE="configure"
