# original url
SRCURL=git://gcc.gnu.org/git/gcc.git
# mirror
#SRCURL=https://gitlab.com/4144/gcc.git

ENV_PATH="bin"
ENV_MANPATH="share/man"

BUILD_TYPE="automake"

CONFIGURE_FLAGS="--enable-languages=c,c++,lto --disable-multilib"
ENV_LD_LIBRARY_PATH="lib:lib64:lib32"
