SRCTYPE=hg
SRCURL=https://hg.icculus.org/icculus/physfs/
DEFAULT_BRANCH="stable-1.0"

ENV_PATH="bin:lib:include"
ENV_LD_LIBRARY_PATH="lib"
ENV_LDFLAGS_PATH="lib"
ENV_CPPFLAGS_PATH="include"

BUILD_TYPE="automake"
SRC_INIT_COMMAND="./bootstrap"
