include "SDL"

DEFAULT_BRANCH="default"

ENV_CPPFLAGS_PATH="include/SDL2"

CONFIGURE_FLAGS="--disable-rpath --enable-sdl-dlopen --disable-loadso \
        --disable-video-ggi \
        --disable-nas --disable-esd --disable-arts \
        --disable-alsa-shared --disable-pulseaudio-shared \
        --disable-x11-shared \
        --disable-video-vulkan \
        --enable-video-directfb"
