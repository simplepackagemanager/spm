SRCURL=git://git.musl-libc.org/musl
DEFAULT_BRANCH="master"

ENV_PATH="bin"
ENV_LD_LIBRARY_PATH="lib"
ENV_LDFLAGS_PATH="lib"
ENV_CPPFLAGS_PATH="include"

BUILD_TYPE="configure"
