SRCURL=https://github.com/zeux/pugixml.git

ENV_PATH="lib:include"
ENV_LD_LIBRARY_PATH="lib"
ENV_LDFLAGS_PATH="lib"
ENV_CPPFLAGS_PATH="include"

BUILD_TYPE="cmake"
