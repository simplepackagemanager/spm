SRCURL=https://github.com/ferzkopp/SDL_gfx.git
DEFAULT_BRANCH="master"

ENV_LD_LIBRARY_PATH="lib"
ENV_PKG_CONFIG_PATH="lib/pkgconfig"
ENV_LDFLAGS_PATH="lib"
ENV_CPPFLAGS_PATH="include/SDL"

ENABLE_SAME_DIR_BUILD="true"

BUILD_TYPE="configure"
SRC_INIT_COMMAND="./autogen.sh"
