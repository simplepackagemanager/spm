SRCURL=git://anongit.freedesktop.org/git/mesa/mesa

ENV_PATH="bin:lib:include"
ENV_LD_LIBRARY_PATH="lib"
ENV_PKG_CONFIG_PATH="lib/pkgconfig"
ENV_LDFLAGS_PATH="lib"
ENV_MANPATH="share/man"
ENV_CPPFLAGS_PATH="include"
ENV_LIBGL_DRIVERS_PATH="lib/dri"

BUILD_TYPE="automake"
CONFIGURE_FLAGS='--enable-gles1 --enable-gles2 --enable-gles3 --with-gallium-drivers="" --with-dri-drivers="i915 i965" --with-vulkan-drivers="intel"'
