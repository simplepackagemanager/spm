SRCURL=git://git.savannah.gnu.org/gettext.git 

ENV_PATH="bin:lib:include:lib/gettext"
ENV_LD_LIBRARY_PATH="lib"
ENV_LDFLAGS_PATH="lib"
ENV_MANPATH="share/man"
ENV_CPPFLAGS_PATH="include"
ENV_ACLOCAL_PATH="share/aclocal"

BUILD_TYPE="automake"
SRC_INIT_COMMAND="./autogen.sh"
