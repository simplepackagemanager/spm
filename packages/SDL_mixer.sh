SRCTYPE=hg
SRCURL=http://hg.libsdl.org/SDL_mixer/
DEFAULT_BRANCH="SDL-1.2"

ENV_LD_LIBRARY_PATH="lib"
ENV_PKG_CONFIG_PATH="lib/pkgconfig"
ENV_LDFLAGS_PATH="lib"
ENV_CPPFLAGS_PATH="include/SDL"

ENABLE_SAME_DIR_BUILD="true"

BUILD_TYPE="configure"
SRC_INIT_COMMAND="./autogen.sh"
CONFIGURE_FLAGS="--disable-music-mp3 \
    --disable-music-mod-shared --disable-music-ogg-shared --disable-music-mp3-shared --disable-music-flac-shared --disable-music-fluidsynth-shared"
