SRCTYPE=hg
SRCURL=http://hg.libsdl.org/SDL/
DEFAULT_BRANCH="SDL-1.2"

ENV_PATH="bin"
ENV_LD_LIBRARY_PATH="lib"
ENV_PKG_CONFIG_PATH="lib/pkgconfig"
ENV_LDFLAGS_PATH="lib"
ENV_MANPATH="share/man"
ENV_ACLOCAL_PATH="share/aclocal"
ENV_CPPFLAGS_PATH="include/SDL"

ENABLE_SAME_DIR_BUILD="true"

BUILD_TYPE="configure"
SRC_INIT_COMMAND="./autogen.sh"
CONFIGURE_FLAGS="--disable-rpath --enable-sdl-dlopen --disable-loadso \
        --disable-video-ggi \
        --disable-nas --disable-esd --disable-arts \
        --disable-alsa-shared --disable-pulseaudio-shared \
        --disable-x11-shared \
        --enable-video-directfb"
