SRCURL=git://git.qemu-project.org/qemu.git

ENV_PATH="bin"
ENV_MANPATH="share/man"

BUILD_TYPE="configure"
CONFIGURE_FLAGS="\
    --target-list=x86_64-linux-user,x86_64-softmmu \
    --disable-nettle \
    --enable-spice \
    --enable-vte \
    --audio-drv-list=sdl,alsa \
    --enable-virglrenderer \
    --enable-sdl \
    --with-sdlabi=2.0"
