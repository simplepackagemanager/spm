SRCURL=git://cmake.org/cmake.git
DEFAULT_BRANCH="v3.7.2"

ENV_PATH="bin"
ENV_ACLOCAL_PATH="share/aclocal"

BUILD_TYPE="cmake"
