SRCURL=https://gitlab.gnome.org/GNOME/libxml2.git

ENV_PATH="bin:lib:include"
ENV_LD_LIBRARY_PATH="lib"
ENV_PKG_CONFIG_PATH="lib/pkgconfig"
ENV_LDFLAGS_PATH="lib"
ENV_MANPATH="share/man"
ENV_ACLOCAL_PATH="share/aclocal"
ENV_CPPFLAGS_PATH="include/libxml2"

BUILD_TYPE="automake"
CONFIGURE_FLAGS="--without-python --with-icu"
