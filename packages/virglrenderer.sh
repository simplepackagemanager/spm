SRCURL=git://people.freedesktop.org/~airlied/virglrenderer

ENV_PATH="bin"
ENV_LD_LIBRARY_PATH="lib"
ENV_PKG_CONFIG_PATH="lib/pkgconfig"
ENV_LDFLAGS_PATH="lib"
ENV_MANPATH="share/man"
ENV_CPPFLAGS_PATH="include:include/virgl"

BUILD_TYPE="automake"
AUTORECONF_FLAGS="-v --install"
