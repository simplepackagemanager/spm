SRCTYPE=hg
SRCURL=https://hg.icculus.org/icculus/physfs/
DEFAULT_BRANCH="stable-2.0"

ENV_PATH="bin:lib:include"
ENV_LD_LIBRARY_PATH="lib"
ENV_PKG_CONFIG_PATH="lib/pkgconfig"
ENV_LDFLAGS_PATH="lib"
ENV_CPPFLAGS_PATH="include"

BUILD_TYPE="cmake"
