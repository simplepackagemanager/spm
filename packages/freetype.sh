SRCURL=git://git.sv.nongnu.org/freetype/freetype2.git

ENV_PATH="bin:lib:include"
ENV_LD_LIBRARY_PATH="lib"
ENV_PKG_CONFIG_PATH="lib/pkgconfig"
ENV_LDFLAGS_PATH="lib"
ENV_MANPATH="share/man"
ENV_ACLOCAL_PATH="share/aclocal"
ENV_CPPFLAGS_PATH="include"

BUILD_TYPE="automake"
SRC_INIT_COMMAND="./autogen.sh"
CONFIGURE_FLAGS="--with-harfbuzz=no"
