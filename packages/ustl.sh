SRCURL=https://github.com/msharov/ustl.git
DEFAULT_BRANCH="master"

ENV_LD_LIBRARY_PATH="lib"
ENV_LDFLAGS_PATH="lib"
ENV_CPPFLAGS_PATH="include"

BUILD_TYPE="configure"

ENABLE_SAME_DIR_BUILD="true"
