#!/bin/bash

export package=$1
source ../scripts/include/common.sh

common_package_init

common_build_init
common_clean_builddir

common_run_package
package_build
