#!/bin/bash

function check_error {
    if [ "$1" != 0 ]; then
        echo "Error detected"
        exit "$1"
    fi
}

function common_build_init {
    export dir=$(pwd)

    if [[ "${installname}" == "" ]]; then
        export installname=${package}
    fi

    if [[ "${envname}" == "" ]]; then
        export envname=${package}
    fi

    if [[ "${MAKE}" == "" ]]; then
        export MAKE="make"
    fi

    export srcdir=$(realpath "${dir}/../src/${package}")
    if [ ! -d "${srcdir}" ]; then
        echo "Error: Package sources directory not exists for package '${package}'."
        exit 3
    fi

    if [ ! -d "../env" ]; then
        mkdir -p "../env"
        check_error $?
    fi
    if [ ! -d "../bin" ]; then
        mkdir -p "../bin"
        check_error $?
    fi
    if [ ! -d "../tmp" ]; then
        mkdir -p "../tmp"
        check_error $?
    fi

    export builddir=$(realpath "${dir}/../tmp/${package}")
    export bindir=$(realpath "${dir}/../bin/${installname}")
}

function common_package_init {
    export scriptsdir=$(realpath .)
    if [[ "${package}" == "" ]]; then
        echo "Error: Missing package name"
        echo "Example: ./build.sh virglrenderer"
        exit 1
    fi

    export packagefile="${package}.sh"
    export SRCTYPE="git"
    export DEFAULT_BRANCH="master"
    export AUTORECONF_FLAGS="-i"

    if [ ! -f "../packages/${packagefile}" ]; then
        echo "Error: Package '${package}' not exists."
        exit 2
    fi
}

function common_run_package {
    cd ../packages
    check_error $?
    echo "${package}.sh"
    source "./${package}.sh"
    check_error $?
}

function env_path {
    echo "export PATH=${bindir}/$1:\$PATH" >>"${envfile}"
    check_error $?
}

function env_aclocal_path {
    echo "export ACLOCAL_PATH=${bindir}/$1:\$PATH" >>"${envfile}"
    check_error $?
}

function env_lib_library_path {
    echo "export LD_LIBRARY_PATH=${bindir}/$1:\$LD_LIBRARY_PATH" >>"${envfile}"
    check_error $?
}

function env_ldflags_path {
    echo "export LDFLAGS=${bindir}/$1 \$LDFLAGS" >>"${envfile}"
    check_error $?
}

function env_cppflags_path {
    echo "export CPPFLAGS=${bindir}/$1 \$CPPFLAGS" >>"${envfile}"
    check_error $?
}

function env_pkg_config_path {
    echo "export PKG_CONFIG_PATH=${bindir}/$1:\$PKG_CONFIG_PATH" >>"${envfile}"
    check_error $?
}

function env_man {
    echo "export MANPATH=${bindir}/$1:\$MANPATH" >>"${envfile}"
    check_error $?
}

function run_autoreconf {
    flags="$*"
    if [[ "${flags}" == "" ]]; then
        flags="${AUTORECONF_FLAGS}"
    fi
    echo "cd ${srcdir}/${SUBDIR}"
    cd "${srcdir}/${SUBDIR}"
    check_error $?
    echo "${MAKE} distclean"
    ${MAKE} distclean
    echo "autoreconf ${flags}"
    autoreconf  ${flags}
    unset flags
    return 0
}

function run_src_script {
    echo "cd ${srcdir}/${SUBDIR}"
    cd "${srcdir}/${SUBDIR}"
    check_error $?
    echo $@
    $@
    check_error $?
}

function run_enable_same_dir_build {
    echo "copy sources from $srcdir to ${builddir}"
    cp -r "$srcdir" "${builddir}/.."
    export srcdir="$builddir"
    echo mkdir -p "${builddir}/${SUBDIR}"
    mkdir -p "${builddir}/${SUBDIR}" || true
    echo "change src dir to $srcdir"
}

function run_configure {
    flags="$*"
    if [[ "${flags}" == "" ]]; then
        flags="${CONFIGURE_FLAGS}"
    fi
    echo "cd ${builddir}/${SUBDIR}"
    cd "${builddir}/${SUBDIR}"
    check_error $?
    echo "$srcdir"/"$SUBDIR"/configure --prefix="${bindir}" "${flags}"
    eval "$srcdir"/"$SUBDIR"/configure --prefix="${bindir}" ${flags}
    check_error $?
    unset flags
}

function run_cmake {
    flags="$*"
    if [[ "${flags}" == "" ]]; then
        flags="${CONFIGURE_FLAGS}"
    fi
    echo "cd ${builddir}/${SUBDIR}"
    cd "${builddir}/${SUBDIR}"
    check_error $?
    echo "cmake -DCMAKE_INSTALL_PREFIX:PATH=\"${bindir}\" \"$srcdir\" ${flags}"
    eval cmake -DCMAKE_INSTALL_PREFIX:PATH="${bindir}" "$srcdir" ${flags}
    check_error $?
    unset flags
}

function run_make {
    if [[ "${jobs}" == "" ]]; then
        if [ -f "/proc/cpuinfo" ]; then
            jobs="$(cat /proc/cpuinfo|grep processor|wc -l)"
            j=" -j${jobs}"
        else
            j=""
        fi
    fi

    echo "${MAKE}$j"
    ${MAKE}$j
    check_error $?
    unset j
}

function run_make_install {
    echo "${MAKE} install"
    ${MAKE} install
    check_error $?
}

function run_git_clone {
    echo "clone git repository $1"
    git clone "$1" "../src/${package}"
    check_error $?
}

function run_hg_clone {
    echo "clone hg repository $1"
    hg clone "$1" "../src/${package}"
    check_error $?
}

function run_clone {
    if [[ "${SRCTYPE}" == "git" ]]; then
        run_git_clone $1
    elif [[ "${SRCTYPE}" == "hg" ]]; then
        run_hg_clone $1
    else
        echo "Error: unknown SRCTYPE: ${SRCTYPE}"
        exit 1
    fi
}

function run_git_switch_branch {
    cd "${srcdir}"
    check_error $?

    if [[ "${srcbranch}" == "" ]]; then
        export srcbranch="$1"
    fi
    echo "git checkout ${srcbranch}"
    git checkout "${srcbranch}"
    check_error $?
}

function run_hg_switch_branch {
    cd "${srcdir}"

    if [[ "${srcbranch}" == "" ]]; then
        export srcbranch="$1"
    fi
    echo "hg update ${srcbranch}"
    hg update "${srcbranch}"
    check_error $?
}

function run_switch_branch {
    branch="$1"
    if [[ "${DEFAULT_BRANCH}" == "" ]]; then
        echo "Error: source branch not set."
        exit 1
    fi
    if [[ "${branch}" == "" ]]; then
        branch="${DEFAULT_BRANCH}"
    fi

    if [[ "${SRCTYPE}" == "git" ]]; then
        run_git_switch_branch ${branch}
    elif [[ "${SRCTYPE}" == "hg" ]]; then
        run_hg_switch_branch ${branch}
    else
        echo "Error: unknown SRCTYPE: ${SRCTYPE}"
        exit 1
    fi
    unset branch
}

function repack_paths {
    envvar="$1"
    if [[ "$2" != "" ]]; then
        envvar="$2"
    fi
    if [[ "$envvar" == "" ]]; then
        return
    fi
    IFS=":"
    packedpaths=""
    for var in $envvar
    do
        packedpaths="${packedpaths}${bindir}/$var:"
    done
    unset IFS
    echo "export $3=\"${packedpaths}\$$3\"" >>"${envfile}"
    check_error $?
}

function repack_flags {
    envvar="$1"
    if [[ "$2" != "" ]]; then
        envvar="$2"
    fi
    if [[ "$envvar" == "" ]]; then
        return
    fi
    IFS=":"
    packedpaths=""
    for var in $envvar
    do
        packedpaths="${packedpaths}${4}${bindir}/$var "
    done
    unset IFS
    echo "export $3=\"${packedpaths}\$$3\"" >>"${envfile}"
    check_error $?
}

function package_use {
    echo "package_use"
    repack_paths "$ENV_PATH" "$OVERRIDE_ENV_PATH" "PATH"
    repack_paths "$ENV_LD_LIBRARY_PATH" "$OVERRIDE_ENV_LD_LIBRARY_PATH" "LD_LIBRARY_PATH"
    repack_paths "$ENV_PKG_CONFIG_PATH" "$OVERRIDE_ENV_PKG_CONFIG_PATH" "PKG_CONFIG_PATH"
    repack_paths "$ENV_MANPATH" "$OVERRIDE_ENV_MANPATH" "MANPATH"
    repack_paths "$ENV_ACLOCAL_PATH" "$OVERRIDE_ENV_ACLOCAL_PATH" "ACLOCAL_PATH"
    repack_paths "$ENV_LIBGL_DRIVERS_PATH" "$OVERRIDE_ENV_LIBGL_DRIVERS_PATH" "LIBGL_DRIVERS_PATH"
    repack_flags "$ENV_LDFLAGS_PATH" "$OVERRIDE_ENV_LDFLAGS_PATH" "LDFLAGS" "-L"
    repack_flags "$ENV_CPPFLAGS_PATH" "$OVERRIDE_ENV_CPPFLAGS_PATH" "CPPFLAGS" "-I"
}

function common_use_package {
    cd "${scriptsdir}"
    check_error $?
    export envfile="../env/run${envname}.sh"
    echo "#!/bin/bash" >"${envfile}"
    check_error $?
    echo "" >>"${envfile}"
    package_use
    check_error $?
    echo "\"\$@\"" >>"${envfile}"
    check_error $?
    chmod 0755 "${envfile}"
    check_error $?
}

function common_use_packages {
    cd "${scriptsdir}"
    check_error $?

    export envname="$1"
    export installname0="${installname}"
    export envfile="../env/run${envname}.sh"
    echo "#!/bin/bash" >"${envfile}"
    check_error $?
    echo "" >>"${envfile}"
    shift
    names="$*"
    IFS=" "
    for package in $names
    do
        export installname="${installname0}"
        unset ENV_PATH
        unset ENV_LD_LIBRARY_PATH
        unset ENV_PKG_CONFIG_PATH
        unset ENV_MANPATH
        unset ENV_ACLOCAL_PATH
        unset ENV_LDFLAGS_PATH
        unset ENV_CPPFLAGS_PATH

        common_package_init
        common_build_init
        common_run_package
        package_use
        check_error $?
    done
    unset IFS
    echo "\"\$@\"" >>"${envfile}"
    check_error $?
    chmod 0755 "${envfile}"
    check_error $?
}

function common_update_source_all {
    cd ../src/
    check_error $?
    files="*"
    for package in $files
    do
        cd ../scripts/
        ./updatesrc.sh $package
        check_error $?
    done
    unset files
}

function common_reset_source_all {
    cd ../src/
    check_error $?
    files="*"
    for package in $files
    do
        cd ../scripts/
        ./resetsrc.sh $package
        check_error $?
    done
    unset files
}

function package_get_source {
    echo "package_get_source"
    run_clone "${SRCURL}"
}

function package_update_source {
    echo "package_update_source"
    cd "../src/${package}"
    if [ -d .git ]; then
        echo "git fetch origin"
        git fetch --all
        check_error $?
        run_switch_branch
        echo "git pull --rebase"
        git pull --rebase --all
        return 0
    fi
    if [ -d .hg ]; then
        echo "hg pull"
        hg pull
        check_error $?
        return
    fi
}

function package_reset_source {
    echo "package_reset_source"
    cd "../src/${package}"
    if [ -d .git ]; then
        echo "git reset --hard HEAD"
        git reset --hard HEAD
        check_error $?
        return
    fi
    if [ -d .hg ]; then
        echo "hg update -C"
        hg update -C
        check_error $?
        return
    fi
}

function common_clean_destination {
    echo "clean ${bindir}"
    rm -rf "${bindir}"
}

function common_clean_builddir {
    echo rm -rf "${builddir}"
    rm -rf "${builddir}"
    check_error $?
    echo mkdir -p "${builddir}"
    mkdir -p "${builddir}"
    check_error $?
}

function package_build {
    echo "package_build"
    run_switch_branch

    if [[ "$ENABLE_SAME_DIR_BUILD" != "" ]]; then
        run_enable_same_dir_build
    fi
    if [[ "$SRC_INIT_COMMAND" != "" ]]; then
        eval run_src_script "$SRC_INIT_COMMAND"
    fi

    case "$BUILD_TYPE" in
        automake)
            run_autoreconf
            run_configure
            ;;
        configure)
            run_configure
            ;;
        cmake)
            run_cmake
            ;;
        *)
            echo "Error: unknown BUILD_TYPE. Valid values: automake, configure, cmake"
            exit 1
            ;;
    esac

    run_make
    run_make_install
}

function include {
    source "./$1.sh"
}
