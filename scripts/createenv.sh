#!/bin/bash

export package=$1
source ../scripts/include/common.sh

common_package_init

common_build_init

common_run_package
common_use_package
